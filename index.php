<!-- Author     : Miguel Cuadros Gálvez 
    CopyLeft 2014-2015 -->
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/materialproject.css"/>
        <link rel="stylesheet" href="css/bootstrap-grid.css"/>
        <link rel="stylesheet" href="css/fonts.css"/>
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="js/materialproject/initialize.js"></script>
        <script>register();</script>
    </head>
    <body class="base-content-light">
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <m-header class="belize-hole t-white">
        <mh-icon class="icon-menu t-white" data-role="lateral-switch"></mh-icon>
        <mh-title></mh-title>
        <mh-options>
            <mh-icon class="icon-news t-white"></mh-icon>
            <mh-icon class="icon-more-vert t-white">
                <m-panel>
                    <a data-modal="#changelog">Changelog</a>
                    <a data-modal="#version">Versión de Desarrollo</a>
                    <hr/>
                    <a data-modal="Acerca de">Acerca de ...</a>
                </m-panel>
            </mh-icon>
        </mh-options>
    </m-header>
    <br/>
    <m-content>
        <span class="display3 t-green-sea">Bienvenido a Material Project!</span><br/>
        <p>Este proyecto esta destinado para todos los que quieran hacer un proyecto web bajo los estándares de <a target="_blank" href="http://google.com/design" class="t-green-sea">Material Design</a>, con la mínima codificación posible, pudiendo así crear aplicaciones web de gran calidad en poco tiempo y responsive. Combina varios frameworks como <a target="_blank" href="http://jquery.com" class="t-green-sea">Jquery</a>, el sistema de grillas de <a target="_blank" href="http://getbootstrap.com" class="t-green-sea">Bootstrap</a>, la paleta de colores de <a target="_blank" href="http://flatuicolors.com" class="t-green-sea">FlatUIColors</a>, y la estandarización de componentes con <a target="_blank" href="http://html5boilerplate.com" class="t-green-sea">HTML5 BoilerPlate</a>.
            <br/>Cualquier feedback por favor hacerlo llegar a <a href="mailto:miguel@bitsandcode.com" class="t-green-sea">miguel@bitsandcode.com</a></p>
        <h2 class="t-belize-hole">Indices</h2>

        <m-index class="b-peter-river t-peter-river">
            <mi-title>Capítulo 1</mi-title>
            <a href="#">Sección 1</a>
            <a href="#">Sección 2</a>
            <mi-title>Capítulo 2</mi-title>
            <a href="#">Sección 3</a>
            <a href="#">Sección 4</a>
        </m-index>

        <h2 class="t-belize-hole">Tipografía y Tamaños</h2>

        <span class="display4">Display 4</span><br/>
        <span class="display3">Display 3</span><br/>
        <span class="display2">Display 2</span><br/>
        <span class="display1">Display 1</span><br/>
        <span class="headline">Headline</span><br/>
        <span class="title">Title</span><br/>
        <span class="subhead">Subhead</span><br/>
        <span class="body2">Body 2</span><br/>
        <span class="body1">Body 1</span><br/>
        <span class="caption">Caption</span><br/>

        <h2 class="t-belize-hole">Colores</h2>
        <div class="container-fluid t-white">
            <div class="row">
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 turquoise">Turquoise</div>
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 emerald">Emerald</div>
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 peter-river">Peter River</div>

            </div>
            <div class="row">
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 amethyst">Amethyst</div>
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 wet-asphalt">Wet Asphalt</div>
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 green-sea">Green Sea</div>

            </div>
            <div class="row">
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 nephrtitis">Nephrtitis</div>
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 belize-hole">Belize Hole</div>
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 wisteria">Wisteria</div>

            </div>
            <div class="row">
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 midnight-blue">Midnight Blue</div>
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 sun-flower">Sun Flower</div>
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 carrot">Carrot</div>

            </div>
            <div class="row">
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 alizarin">Alizarin</div>
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 clouds t-darkgray">Clouds</div>
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 concrete">Concrete</div>
            </div>
            <div class="row">
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 orange">orange</div>
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 pumpking">Pumpking</div>
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 clouds pomergranate">Pomergranate</div>
            </div>
            <div class="row">
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 silver">Silver</div>
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 asbestos">Asbestos</div>
                <div style="height:64px; text-transform: uppercase;" class="col-lg-4 base-content t-darkgray">Base Content</div>
            </div>
        </div>
        <br/>
        <h2 class="t-belize-hole">Tarjetas</h2>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-6" >
                    <m-card class="info-card xs-card">
                        <mc-image data-image="img/preview.jpg"></mc-image>
                        <mc-info>
                            <mc-options>
                                <mc-icon class="icon-more-vert"></mc-icon>
                            </mc-options>
                            <mc-title>Firetorch Image</mc-title>
                            <mc-subtitle>Lun 08/12/2014</mc-subtitle>
                        </mc-info>
                    </m-card>
                    <m-card class="info-card xs-card">
                        <mc-image data-image="img/preview2.jpg"></mc-image>
                        <mc-info>
                            <mc-options>
                                <mc-icon class="icon-more-vert"></mc-icon>
                            </mc-options>
                            <mc-title>Material Background</mc-title>
                            <mc-subtitle>Mar 09/12/2014</mc-subtitle>
                        </mc-info>
                    </m-card>
                    <m-card class="info-card xs-card">
                        <mc-image data-image="img/preview3.jpg"></mc-image>
                        <mc-info>
                            <mc-options>
                                <mc-icon class="icon-more-vert"></mc-icon>
                            </mc-options>
                            <mc-title>Flat Polygon Cellphone</mc-title>
                            <mc-subtitle>Mie 10/12/2014</mc-subtitle>
                        </mc-info>
                    </m-card>
                    <m-card class="info-card xs-card">
                        <mc-image data-image="img/preview.jpg"></mc-image>
                        <mc-info>
                            <mc-options>
                                <mc-icon class="icon-more-vert"></mc-icon>
                            </mc-options>
                            <mc-title>Firetorch Image</mc-title>
                            <mc-subtitle>Lun 08/12/2014</mc-subtitle>
                        </mc-info>
                    </m-card>
                    <m-card class="info-card xs-card">
                        <mc-image data-image="img/preview2.jpg"></mc-image>
                        <mc-info>
                            <mc-options>
                                <mc-icon class="icon-more-vert"></mc-icon>
                            </mc-options>
                            <mc-title>Material Background</mc-title>
                            <mc-subtitle>Mar 09/12/2014</mc-subtitle>
                        </mc-info>
                    </m-card>
                    <m-card class="info-card xs-card">
                        <mc-image data-image="img/preview3.jpg"></mc-image>
                        <mc-info>
                            <mc-options>
                                <mc-icon class="icon-more-vert"></mc-icon>
                            </mc-options>
                            <mc-title>Flat Polygon Cellphone</mc-title>
                            <mc-subtitle>Mie 10/12/2014</mc-subtitle>
                        </mc-info>
                    </m-card><m-card class="info-card xs-card">
                        <mc-image data-image="img/preview.jpg"></mc-image>
                        <mc-info>
                            <mc-options>
                                <mc-icon class="icon-more-vert"></mc-icon>
                            </mc-options>
                            <mc-title>Firetorch Image</mc-title>
                            <mc-subtitle>Lun 08/12/2014</mc-subtitle>
                        </mc-info>
                    </m-card>
                    <m-card class="info-card xs-card">
                        <mc-image data-image="img/preview2.jpg"></mc-image>
                        <mc-info>
                            <mc-options>
                                <mc-icon class="icon-more-vert"></mc-icon>
                            </mc-options>
                            <mc-title>Material Background</mc-title>
                            <mc-subtitle>Mar 09/12/2014</mc-subtitle>
                        </mc-info>
                    </m-card>
                    <m-card class="info-card xs-card">
                        <mc-image data-image="img/preview3.jpg"></mc-image>
                        <mc-info>
                            <mc-options>
                                <mc-icon class="icon-more-vert"></mc-icon>
                            </mc-options>
                            <mc-title>Flat Polygon Cellphone</mc-title>
                            <mc-subtitle>Mie 10/12/2014</mc-subtitle>
                        </mc-info>
                    </m-card>
                    <m-card class="info-card xs-card">
                        <mc-image data-image="img/preview.jpg"></mc-image>
                        <mc-info>
                            <mc-options>
                                <mc-icon class="icon-more-vert"></mc-icon>
                            </mc-options>
                            <mc-title>Firetorch Image</mc-title>
                            <mc-subtitle>Lun 08/12/2014</mc-subtitle>
                        </mc-info>
                    </m-card>
                    <m-card class="info-card xs-card">
                        <mc-image data-image="img/preview2.jpg"></mc-image>
                        <mc-info>
                            <mc-options>
                                <mc-icon class="icon-more-vert"></mc-icon>
                            </mc-options>
                            <mc-title>Material Background</mc-title>
                            <mc-subtitle>Mar 09/12/2014</mc-subtitle>
                        </mc-info>
                    </m-card>
                    <m-card class="info-card xs-card">
                        <mc-image data-image="img/preview3.jpg"></mc-image>
                        <mc-info>
                            <mc-options>
                                <mc-icon class="icon-more-vert"></mc-icon>
                            </mc-options>
                            <mc-title>Flat Polygon Cellphone</mc-title>
                            <mc-subtitle>Mie 10/12/2014</mc-subtitle>
                        </mc-info>
                    </m-card>
                </div>
                <div class="col-xs-6" style="padding-left: 4px; padding-right: 4px;">
                    <m-component>
                        <mc-image data-image="img/preview.jpg"></mc-image>
                        <mc-header>
                            <mc-actionbutton class="icon-add t-white"></mc-actionbutton>
                            <mc-title>Firetorch Image</mc-title>
                            <mc-subtitle>Subtitulo de Prueba</mc-subtitle>
                        </mc-header>
                        <mc-content>
                            <mm-picture data-image="img/avatar.jpg"></mm-picture>
                            <p>
                                <span class="author">Escrito por Miguel Cuadros</span><br/>
                                <span class="date">Vie 12/12/2014</span>
                            </p>
                            <article>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse faucibus erat magna, quis rutrum tellus venenatis et. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut euismod, orci congue sodales ornare, ex turpis venenatis erat, efficitur placerat massa libero non quam. Duis rutrum risus at erat tristique pellentesque. Aenean vitae turpis arcu. Integer ut ultrices massa. In vitae ullamcorper ipsum, vitae luctus sem. Curabitur non euismod tellus. Mauris commodo volutpat egestas. Phasellus rutrum metus eu purus ullamcorper congue. Nullam ut sapien tristique, malesuada metus at, suscipit nibh. Integer vitae varius dolor, a lobortis mauris. Integer fermentum pharetra aliquet. Ut facilisis lorem nisi. Pellentesque tempus ex ut sem lobortis, malesuada sodales elit aliquam. Maecenas volutpat vitae ipsum et convallis.
                                <br/><br/>
                                Aenean viverra nulla a diam placerat, ut euismod est porttitor. Fusce sollicitudin ipsum eu elit fringilla, in pulvinar nisl laoreet. Vestibulum vel semper odio. Vestibulum non diam gravida, blandit est ultrices, sodales neque. Proin id sapien sed ipsum mollis varius vel sed odio. Nunc feugiat tortor turpis, a fringilla nisl lobortis quis. Fusce laoreet, tortor non sagittis dignissim, nulla quam auctor velit, id congue odio odio sit amet quam. Praesent vitae iaculis est, et mollis ante. Pellentesque vulputate placerat nisl, vel rutrum eros venenatis at. Ut scelerisque eleifend justo, et elementum lectus tempor vel. Donec mattis mattis purus non feugiat.
                            </article>
                        </mc-content>
                    </m-component>
                    <br/>
                </div>
            </div>
        </div>
        <m-component>
            <mc-image data-image="img/preview.jpg"></mc-image>
            <mc-header>
                <mc-actionbutton class="icon-add t-white"></mc-actionbutton>
                <mc-title>Firetorch Image</mc-title>
                <mc-subtitle>Subtitulo de Prueba</mc-subtitle>
            </mc-header>
            <mc-content>
                <mm-picture data-image="img/avatar.jpg"></mm-picture>
                <p>
                    <span class="author">Escrito por Miguel Cuadros</span><br/>
                    <span class="date">Vie 12/12/2014</span>
                </p>
                <article>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse faucibus erat magna, quis rutrum tellus venenatis et. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut euismod, orci congue sodales ornare, ex turpis venenatis erat, efficitur placerat massa libero non quam. Duis rutrum risus at erat tristique pellentesque. Aenean vitae turpis arcu. Integer ut ultrices massa. In vitae ullamcorper ipsum, vitae luctus sem. Curabitur non euismod tellus. Mauris commodo volutpat egestas. Phasellus rutrum metus eu purus ullamcorper congue. Nullam ut sapien tristique, malesuada metus at, suscipit nibh. Integer vitae varius dolor, a lobortis mauris. Integer fermentum pharetra aliquet. Ut facilisis lorem nisi. Pellentesque tempus ex ut sem lobortis, malesuada sodales elit aliquam. Maecenas volutpat vitae ipsum et convallis.
                    <br/><br/>
                    Aenean viverra nulla a diam placerat, ut euismod est porttitor. Fusce sollicitudin ipsum eu elit fringilla, in pulvinar nisl laoreet. Vestibulum vel semper odio. Vestibulum non diam gravida, blandit est ultrices, sodales neque. Proin id sapien sed ipsum mollis varius vel sed odio. Nunc feugiat tortor turpis, a fringilla nisl lobortis quis. Fusce laoreet, tortor non sagittis dignissim, nulla quam auctor velit, id congue odio odio sit amet quam. Praesent vitae iaculis est, et mollis ante. Pellentesque vulputate placerat nisl, vel rutrum eros venenatis at. Ut scelerisque eleifend justo, et elementum lectus tempor vel. Donec mattis mattis purus non feugiat.
                </article>
            </mc-content>
        </m-component>
        <br/>
        <br/>
    </m-content>
    <m-mainaside data-role="lateral-switch">
        <m-panel>
            <mm-header data-image="img/preview2.jpg" class="extended-header">
                <mm-picture class="avatar" data-image="img/avatar.jpg"></mm-picture>
                <mm-subtitle class="t-white">miguel@bitsandcode.com 
                    <mm-icon class="icon-arrow-drop-down t-white"></mm-icon>
                </mm-subtitle>
            </mm-header>
            <mm-content>
                <a>Bandeja</a>
                <a>Inicio</a>
                <a>Mensajes Enviados</a>
                <a>Borradores</a>
                <hr/>
                <a><span class="icon-inbox"></span>Todos los Mensajes</a>
                <a><span class="icon-delete"></span>Basura</a>
                <a><span class="icon-mail"></span>Spam</a>
                <a><span class="icon-beenhere"></span>Siguiendo</a>
            </mm-content>
        </m-panel>
    </m-mainaside>
    <m-modal id="changelog">
        <m-panel >
            <mc-title>Changelog</mc-title>
            <mc-content>
                Este proyecto esta destinado para todos los que quieran hacer un proyecto web bajo los estándares de Material Design, con la mínima codificación posible, pudiendo así crear aplicaciones web de gran calidad en poco tiempo y responsive. Combina varios frameworks como Jquery, el sistema de grillas de Bootstrap, la paleta de colores de FlatUIColors, y la estandarización de componentes con HTML5 BoilerPlate. 
                Cualquier feedback por favor hacerlo llegar a miguel@bitsandcode.com

            </mc-content>
            <hr/>
            <mc-options>
                <button type="button" data-modal="#changelog">ENTENDIDO</button>
            </mc-options>
        </m-panel>
    </m-modal>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
    <script>
            initialize();
            $(document).ready(function () {
                var ver = "0.0.620a";
                $('mh-title').html("Material Project - " + ver);
            });
    </script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function (b, o, i, l, e, r) {
            b.GoogleAnalyticsObject = l;
            b[l] || (b[l] =
                    function () {
                        (b[l].q = b[l].q || []).push(arguments)
                    });
            b[l].l = +new Date;
            e = o.createElement(i);
            r = o.getElementsByTagName(i)[0];
            e.src = '//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e, r)
        }(window, document, 'script', 'ga'));
        ga('create', 'UA-XXXXX-X');
        ga('send', 'pageview');
    </script>
</body>
</html>
