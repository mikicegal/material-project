/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var mcImage;
var mhTitle;
var mmHeader;
var mMainAside;
var mhOptions;
var mhIcon;
var mPanel;
var mContent;
var mIndex;
var mCard;
var mcInfo;
var mcTitle;
var mcSubtitle;
var mcOptions;
var mmPicture;
var mmContent;
var mComponent;
var mcActionButton;

function register() {
    mcImage = document.registerElement('mc-image', {
        prototype: Object.create(HTMLElement.prototype)
    });
    mhTitle = document.registerElement('mh-title', {
        prototype: Object.create(HTMLElement.prototype)
    });
    mmHeader = document.registerElement('mm-header', {
        prototype: Object.create(HTMLElement.prototype)
    });
    mMainAside = document.registerElement('m-mainaside', {
        prototype: Object.create(HTMLElement.prototype)
    });
    mhOptions = document.registerElement('mh-options', {
        prototype: Object.create(HTMLElement.prototype)
    });
    mhIcon = document.registerElement('mh-icon', {
        prototype: Object.create(HTMLElement.prototype)
    });
    mPanel = document.registerElement('m-panel', {
        prototype: Object.create(HTMLElement.prototype)
    });
    mContent = document.registerElement('m-content', {
        prototype: Object.create(HTMLElement.prototype)
    });
    mIndex = document.registerElement('m-index', {
        prototype: Object.create(HTMLElement.prototype)
    });
    mCard = document.registerElement('m-card', {
        prototype: Object.create(HTMLElement.prototype)
    });
    mcInfo = document.registerElement('mc-info', {
        prototype: Object.create(HTMLElement.prototype)
    });
    mcTitle = document.registerElement('mc-title', {
        prototype: Object.create(HTMLElement.prototype)
    });
    mcSubtitle = document.registerElement('mc-subtitle', {
        prototype: Object.create(HTMLElement.prototype)
    });
    mcOptions = document.registerElement('mc-options', {
        prototype: Object.create(HTMLElement.prototype)
    });
    mmPicture = document.registerElement('mm-picture', {
        prototype: Object.create(HTMLElement.prototype)
    });
    mmContent = document.registerElement('mm-content', {
        prototype: Object.create(HTMLElement.prototype)
    });
    mComponent = document.registerElement('m-component', {
        prototype: Object.create(HTMLElement.prototype)
    });
    mcActionButton = document.registerElement('mc-actionbutton', {
        prototype: Object.create(HTMLElement.prototype)
    });
}

function switchMenu() {
    if ($('m-mainaside').is(':visible')) {
        $('m-mainaside > m-panel').css({'transform': "translateX(-300px)", 'transition': "all .5s"});
        $('m-mainaside').fadeOut(500);
        $('body').css({'overflow': 'auto'});
    } else {
        $('m-mainaside').fadeIn(500);
        $('m-mainaside > m-panel').css({'transform': "translateX(0px)", 'transition': "all .5s"});
        $('body').css({'overflow': 'hidden'});
    }
    $("m-mainaside > m-panel").click(function (e) {
        e.stopPropagation();
    });

}

function switchModal(e) {
    if ($(e.data.idModal).is(':visible')) {
        $(e.data.idModal).fadeOut(200);
        $('body').css({'overflow': 'auto'});
    } else {
        $(e.data.idModal).fadeIn(200);
        $('body').css({'overflow': 'hidden'});
    }
    $(e.data.idModal + " > m-panel").click(function (e) {
        e.stopPropagation();
    });
}

function initialize() {

    var total = $(document).find("[data-image]");
    for (var i = 0; i < total.length; i++) {
        $(total[i]).css({'background-image': "url(" + $(total[i]).attr("data-image") + ")", 'background-size': 'cover', 'background-position': 'center'});
    }

    var switchs = $(document).find("[data-role='lateral-switch']");
    for (var i = 0; i < switchs.length; i++) {
        $(switchs[i]).click(switchMenu);
    }
    
    var modals = $(document).find("[data-modal]");
    for (var i = 0; i < modals.length; i++) {
        $(modals[i]).click({idModal: $(modals[i]).attr("data-modal")},switchModal);
    }

}